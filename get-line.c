#define _GNU_SOURCE

#include "get-line.h"
#include <string.h>

ssize_t get_delim (struct gl_line *line, char delim) {
	return getdelim (&line->buf, &line->a, delim, line->f);
}

ssize_t get_line (struct gl_line *line) {
	return getline (&line->buf, &line->a, line->f);
}

ssize_t get_delim_strip (struct gl_line *line, char delim) {
	ssize_t r = getdelim (&line->buf, &line->a, delim, line->f);
	if (r > 1 && line->buf[r - 1] == delim)
		line->buf[r - 1] = 0;
	return r;
}

ssize_t get_line_strip (struct gl_line *line) {
	ssize_t r = getline (&line->buf, &line->a, line->f);
	if (r > 1 && line->buf[r - 1] == '\n')
		line->buf[r - 1] = 0;
	return r;
}

struct gl_line make_line (char *path, enum gl_line_flags flags) {
	struct gl_line r = {
		.path = path,
		.flags = flags,
	};
	if (flags & GLF_IS_PIPE)
		r.f = popen (path, "r");
	else if (*path == '-' && *(path + 1) == 0)
		r.f = stdin;
	else
		r.f = fopen (path, "r");
	return r;
}

// This returns a FILE *. It only does that so you can do
// "if !open_line". The point of this library is convenience.
FILE *open_line (struct gl_line *line) {
	if (line->flags & GLF_IS_PIPE)
		line->f = popen (line->path, "r");
	else if (*line->path == '-' && *(line->path + 1) == 0)
		line->f = stdin;
	else
		line->f = fopen (line->path, "r");
	return line->f;
}

int close_line (struct gl_line *line) {
	int r = 0;
	if (line->flags & GLF_IS_PIPE)
		r = pclose (line->f);
	else if (line->f != stdin)
		r = fclose (line->f);

	// FIXME? Here we set line->f to 0. Perhaps we shouldn't do that
	// if f- or pclose has failed?
	line->f = 0;
	return r;
}


// Note we don't actually nuke the file path. It makes no sense to do
// it since the library didn't allocate it. Furthermore, if we free it
// here and return an error message, how would you print an error
// message.
int nuke_line (struct gl_line *line) {
	free (line->buf);

	int r = close_line (line);

	// DON'T GET RID OF ->path! It's used for error messages, and it's
	// not allocated by this lib.

	return r;
}
