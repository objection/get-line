#pragma once

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

enum gl_line_flags {

	// Use popen instead of fopen to open the FILE *
	GLF_IS_PIPE = 1 << 0,
};
typedef enum gl_line_flags gl_line_flags;

typedef struct gl_line gl_line;
struct gl_line {
	char *buf;
	FILE *f;
	char *path;
	size_t a;

	enum gl_line_flags flags;
};

struct gl_line make_line (char *path, enum gl_line_flags flags);
int nuke_line (struct gl_line *line);
FILE *open_line (struct gl_line *line);
int close_line (struct gl_line *line);
ssize_t get_delim (struct gl_line *line, char delim);
ssize_t get_line (struct gl_line *line);
ssize_t get_delim_strip (struct gl_line *line, char delim);
ssize_t get_line_strip (struct gl_line *line);
