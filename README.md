# Get Line

This is just a straightforward wrapper around getline to make it
not clutter the code so much.

The problem with getline is it requires you to declare four
variables:

* char \*\*lineptr: The buffer you want the line to go in.
* size_t * n: Getline will update this with the new size of the buffer.
* FILE *stream: The file, obviously.
* An a ssize_t for the return value. You don't always have to bother
	with this.

A lot of work. I'm getting puffed out just thinking about it.

This (very small) library cuts all that down to this.

```c
	struct gl_line line = make_line ("/tmp/this.c", 0);
	if (!line.f)
		err (1, "Couldn't open %s", line.path);
	while (-1 != get_line (&line))
		printf ("Hi, \"%s\"\n", line.buf);
	nuke_line (&line)
```
# Data structures

## struct line

Here's the struct line in all of its glory.

```c
struct line {
	char *buf; 	// The line itself.
	size_t a;  	// How many bytes are allocated
	FILE *f;	// The FILE *.
	char *path; // A path if not flags & GLF_IS_PIPE, else a shell
				// command.
	enum gl_line_flags flags;
};

enum gl_line_flags {

	// Free the path when calling nuke. Without this, it's left
	// alone.
	GLF_PATH_FREE_WHEN_NUKE = 1 << 0,

	// Use popen instead of fopen to open the FILE *
	GLF_IS_PIPE = 1 << 1,
};

```

# Functions

## make_line

```c
struct line make_line (char *path, enum gl_line_flags flags);
```

make_line returns a line. It opens the file, and sticks "path" in
gl_line.path, and opens it.

By default "opens it" means calling fopen. If flags & GLF_IS_PIPE,
make_line will call popen.

gl_line.path isn't strduped, unless flags & GLF_PATH_FREE_WHEN_NUKE.

You can tell if the call has failed by checking the FILE *, gl_line.f.
It'll be 0 if it has.

gl_line.buf will be 0 at this point. getline will fill it.

A gl_line.path of "-" will open stdin. I dunno if this is great
design. If I was doing it properly I'd need to let you escape it. To
be honest, probably a flag would be a better choice.

## close_line

```c
int close_line (struct gl_line *line) {
```

Closes the FILE *. If flags & GLF_IS_PIPE, it does that by calling
pclose.

The return value is the result of pclose, or fclose. pclose, btw,
gives you the exit status of the cmd.

## nuke_line

```c
int nuke_line (struct gl_line *line);
```

This destroys a line, freeing .buf, fclose or pclosing the FILE *,
depending on whether it was a pipe or not.

If gl_line.flags & GLF_PATH_FREE_WHEN_NUKE, nuke_line will free
line->path.

Trickiness: if the FILE * can't be closed, gl_line.path won't be
freed. This is so you can print out an error message.

## get_line

```c
ssize_t get_line (struct gl_line *line);
```

This is just getline, except it takes one parameter instead of three,
a pointer to your line struct.

## get_delim

```c
ssize_t get_delim (struct line *line, char delim);
```

This is just getdelim, except it takes two parameters instead of four,
a pointer to your line struct and the delim to get.

## get_line_strip, get_delim_strip

These are what they sound like. getline includes the newline in the
buffer. These chop it out.

